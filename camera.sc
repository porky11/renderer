using import struct
using import utils.utils
using import ga

inline Camera (D ET)
    struct (generic-name "Camera" D ET) plain
        pos : (vec D ET)
        dir : (rotor D ET)

locals;
