using import .shaders
using import .space
using import .camera
using import .shapes

using import struct

import wgpu
import .glfw
using import ga

let camera-type = (Camera D ET)

let window-size-type = (vector f32 2)

let shape-type = (Sphere D ET)

include (import C) "unistd.h"

struct Keys
    a : bool
    d : bool
    w : bool
    s : bool
    q : bool
    e : bool

global pressed : Keys

fn main ()
    let instance =
        wgpu.Instance;

    let adapter =
        wgpu.Adapter instance
            local wgpu.AdapterDescriptor
                power_preference = wgpu.PowerPreference.LowPower

    let device =
        wgpu.Device adapter
            local wgpu.DeviceDescriptor
                extensions =
                    typeinit
                        anisotropic_filtering = false

    let vertex-shader =
        wgpu.ShaderModule device
            local wgpu.ShaderModuleDescriptor
                code = vert

    let fragment-shader =
        wgpu.ShaderModule device
            local wgpu.ShaderModuleDescriptor
                code = frag

    local global-layout-bindings =
        arrayof wgpu.BindGroupLayoutBinding
            typeinit
                binding = 0
                visibility = wgpu.ShaderStage.FRAGMENT
                ty = wgpu.BindingType.UniformBuffer
            typeinit
                binding = 1
                visibility = wgpu.ShaderStage.FRAGMENT
                ty = wgpu.BindingType.UniformBuffer

    local object-layout-bindings =
        arrayof wgpu.BindGroupLayoutBinding
            typeinit
                binding = 0
                visibility = wgpu.ShaderStage.FRAGMENT
                ty = wgpu.BindingType.UniformBuffer
            typeinit
                binding = 1
                visibility = wgpu.ShaderStage.FRAGMENT
                ty = wgpu.BindingType.UniformBuffer
            typeinit
                binding = 0x10
                visibility = wgpu.ShaderStage.FRAGMENT
                ty = wgpu.BindingType.StorageBuffer
            typeinit
                binding = 0x11
                visibility = wgpu.ShaderStage.FRAGMENT
                ty = wgpu.BindingType.StorageBuffer

    let global-bind-group-layout =
        wgpu.BindGroupLayout device
            local wgpu.BindGroupLayoutDescriptor global-layout-bindings

    let object-bind-group-layout =
        wgpu.BindGroupLayout device
            local wgpu.BindGroupLayoutDescriptor object-layout-bindings

    let window-size-buffer window-size-data =
        wgpu.Buffer device
            local wgpu.BufferDescriptor
                size = (sizeof window-size-type)
                usage =
                    wgpu.BufferUsage.UNIFORM
            window-size-type

    let camera-buffer camera-data =
        wgpu.Buffer device
            local wgpu.BufferDescriptor
                size = (sizeof camera-type)
                usage =
                    wgpu.BufferUsage.UNIFORM
            camera-type

    let count-buffer1 count-data1 =
        wgpu.Buffer device
            local wgpu.BufferDescriptor
                size =
                    sizeof u32
                usage =
                    wgpu.BufferUsage.STORAGE
            u32

    let shape-buffer1 shape-data1 =
        wgpu.Buffer device
            local wgpu.BufferDescriptor
                size =
                    0x100 * (sizeof shape-type)
                usage =
                    wgpu.BufferUsage.STORAGE
            shape-type

    let count-buffer2 count-data2 =
        wgpu.Buffer device
            local wgpu.BufferDescriptor
                size =
                    sizeof u32
                usage =
                    wgpu.BufferUsage.STORAGE
            u32

    let shape-buffer2 shape-data2 =
        wgpu.Buffer device
            local wgpu.BufferDescriptor
                size =
                    0x100 * (sizeof shape-type)
                usage =
                    wgpu.BufferUsage.STORAGE
            shape-type


    local shapes =
        arrayof shape-type
            typeinit
                center =
                    vecof ET 2 0 -2
            typeinit
                center =
                    vecof ET -2 0 -2
            typeinit
                center =
                    vecof ET 0 2 -2
            typeinit
                center =
                    vecof ET 0 -2 -2

    @count-data1 = (countof shapes)

    shape-data1 @ 0 = shapes @ 0
    shape-data1 @ 1 = shapes @ 1
    shape-data1 @ 2 = shapes @ 2
    shape-data1 @ 3 = shapes @ 3

    @count-data2 = (countof shapes)

    shape-data2 @ 0 = shapes @ 0
    shape-data2 @ 1 = shapes @ 1
    shape-data2 @ 2 = shapes @ 2
    shape-data2 @ 3 = shapes @ 3

    'unmap count-buffer1
    'unmap shape-buffer1
    'unmap count-buffer2
    'unmap shape-buffer2

    local global-bindings =
        arrayof wgpu.BindGroupBinding
            typeinit
                binding = 0
                resource =
                    wgpu.BindingResource.Buffer
                        local wgpu.BufferBinding
                            buffer = window-size-buffer
                            offset = 0
                            size = (sizeof window-size-type)
            typeinit
                binding = 1
                resource =
                    wgpu.BindingResource.Buffer
                        local wgpu.BufferBinding
                            buffer = camera-buffer
                            offset = 0
                            size = (sizeof camera-type)

    local object-bindings1 =
        arrayof wgpu.BindGroupBinding
            typeinit
                binding = 0
                resource =
                    wgpu.BindingResource.Buffer
                        local wgpu.BufferBinding
                            buffer = window-size-buffer
                            offset = 0
                            size = (sizeof window-size-type)
            typeinit
                binding = 1
                resource =
                    wgpu.BindingResource.Buffer
                        local wgpu.BufferBinding
                            buffer = camera-buffer
                            offset = 0
                            size = (sizeof camera-type)
            typeinit
                binding = 0x10
                resource =
                    wgpu.BindingResource.Buffer
                        local wgpu.BufferBinding
                            buffer = count-buffer1
                            offset = 0
                            size = (sizeof u32)
            typeinit
                binding = 0x11
                resource =
                    wgpu.BindingResource.Buffer
                        local wgpu.BufferBinding
                            buffer = shape-buffer1
                            offset = 0
                            size =
                                0x100 * (sizeof shape-type)

    local object-bindings2 =
        arrayof wgpu.BindGroupBinding
            typeinit
                binding = 0
                resource =
                    wgpu.BindingResource.Buffer
                        local wgpu.BufferBinding
                            buffer = window-size-buffer
                            offset = 0
                            size = (sizeof window-size-type)
            typeinit
                binding = 1
                resource =
                    wgpu.BindingResource.Buffer
                        local wgpu.BufferBinding
                            buffer = camera-buffer
                            offset = 0
                            size = (sizeof camera-type)
            typeinit
                binding = 0x10
                resource =
                    wgpu.BindingResource.Buffer
                        local wgpu.BufferBinding
                            buffer = count-buffer2
                            offset = 0
                            size = (sizeof u32)
            typeinit
                binding = 0x11
                resource =
                    wgpu.BindingResource.Buffer
                        local wgpu.BufferBinding
                            buffer = shape-buffer2
                            offset = 0
                            size =
                                0x100 * (sizeof shape-type)

    let global-bind-group =
        wgpu.BindGroup device
            local wgpu.BindGroupDescriptor
                layout = global-bind-group-layout
                bindings = global-bindings

    let object-bind-group1 =
        wgpu.BindGroup device
            local wgpu.BindGroupDescriptor
                layout = object-bind-group-layout
                bindings = object-bindings1

    let object-bind-group2 =
        wgpu.BindGroup device
            local wgpu.BindGroupDescriptor
                layout = object-bind-group-layout
                bindings = object-bindings2


    local bind-group-layouts =
        arrayof wgpu.BindGroupLayout
            object-bind-group-layout
            global-bind-group-layout

    let pipeline-layout =
        wgpu.PipelineLayout device
            local wgpu.PipelineLayoutDescriptor
                bind_group_layouts = bind-group-layouts

    let render-pipeline =
        wgpu.RenderPipeline device
            local wgpu.RenderPipelineDescriptor
                layout = pipeline-layout
                vertex_stage =
                    wgpu.PipelineStageDescriptor
                        module = vertex-shader
                        entry_point = "main"
                fragment_stage =
                    local wgpu.PipelineStageDescriptor
                        module = fragment-shader
                        entry_point = "main"
                rasterization_state =
                    wgpu.RasterizationStateDescriptor
                        front_face = wgpu.FrontFace.Ccw
                        cull_mode = wgpu.CullMode.None
                        depth_bias = 0
                        depth_bias_slope_scale = 0.0
                        depth_bias_clamp = 0.0
                primitive_topology = wgpu.PrimitiveTopology.TriangleList
                color_states =
                    local wgpu.ColorStateDescriptor
                        format = wgpu.TextureFormat.Bgra8Unorm
                        alpha_blend =
                            wgpu.BlendDescriptor
                                src_factor = wgpu.BlendFactor.One
                                dst_factor = wgpu.BlendFactor.Zero
                                operation = wgpu.BlendOperation.Add
                        color_blend =
                            wgpu.BlendDescriptor
                                src_factor = wgpu.BlendFactor.One
                                dst_factor = wgpu.BlendFactor.Zero
                                operation = wgpu.BlendOperation.Add
                        write_mask = wgpu.ColorWrite.ALL
                depth_stencil_state = null
                vertex_input =
                    wgpu.VertexInputDescriptor
                        index_format = wgpu.IndexFormat.Uint16
                sample_count = 1

    if (not (glfw.init))
        raise glfw.Error.Init
    defer glfw.terminate

    glfw.windowHint glfw.CLIENT_API glfw.NO_API

    let window =
        glfw.createWindow 640 480 "wgpu with glfw" null null
    if (window == null)
        raise glfw.Error.Window
    defer glfw.destroyWindow window

    glfw.setKeyCallback window
        fn (window key scancode action mods)
            match key
            case glfw.KEY_W
                pressed.w = action
            case glfw.KEY_A
                pressed.a = action
            case glfw.KEY_S
                pressed.s = action
            case glfw.KEY_D
                pressed.d = action
            case glfw.KEY_Q
                pressed.q = action
            case glfw.KEY_E
                pressed.e = action
            default;

    local camera : camera-type
        dir =
            as
                scalof ET 1
                rotor D ET

    let surface =
        do
            let x11_display = (glfw.getX11Display)
            let x11_window = (glfw.getX11Window window)
            wgpu.Surface instance 'xlib
                bitcast x11_display (mutable pointer voidstar)
                x11_window

    local prev_width = 0
    local prev_height = 0

    local swap-chain =
        wgpu.SwapChain device surface
            local wgpu.SwapChainDescriptor
                usage = wgpu.TextureUsage.OUTPUT_ATTACHMENT
                format = wgpu.TextureFormat.Bgra8Unorm
                width =
                    prev_width as u32
                height =
                    prev_height as u32
                present_mode = wgpu.PresentMode.NoVsync

    let queue =
        wgpu.Queue device

    let fps = 24

    loop (frame-number = 0)
        if (glfw.windowShouldClose window)
            break;
        let start-time =
            glfw.getTime;

        let cmd-encoder =
            wgpu.CommandEncoder device
                local wgpu.CommandEncoderDescriptor
                    todo = 0

        local cmd-buf : wgpu.CommandBuffer cmd-encoder

        local width = 0
        local height = 0
        glfw.getWindowSize window &width &height

        local xpos : f64
        local ypos : f64
        glfw.getCursorPos window &xpos &ypos
        
        xpos -= width / 2
        ypos -= height / 2

        camera.dir *=
            'normalize
                (vecof ET 0 0 0x4000) as (vec D ET) * (vecof ET xpos ypos 0x4000) as (vec D ET)

        camera.pos +=
            'geometric
                *
                    camera.dir
                    0x0.4 as ET *
                        vecof ET
                            pressed.d as ET - pressed.a as ET
                            pressed.q as ET - pressed.e as ET
                            pressed.s as ET - pressed.w as ET
                'reverse camera.dir
                vec D ET

        @camera-data = camera

        if (width != prev_width or height != prev_height)
            prev_width = width
            prev_height = height

            swap-chain =
                wgpu.SwapChain device surface
                    local wgpu.SwapChainDescriptor
                        usage = wgpu.TextureUsage.OUTPUT_ATTACHMENT
                        format = wgpu.TextureFormat.Bgra8Unorm
                        width = (width as u32)
                        height = (height as u32)
                        present_mode = wgpu.PresentMode.NoVsync

            @window-size-data =
                vectorof f32 width height

        let next-texture =
            wgpu.SwapChainOutput swap-chain


        local color-attachment : wgpu.RenderPassColorAttachmentDescriptor
            attachment = next-texture.view_id
            load_op = wgpu.LoadOp.Load
            store_op = wgpu.StoreOp.Store
            clear_color = wgpu.Color.GREEN

        let rpass =
            wgpu.RenderPass cmd-encoder
                local wgpu.RenderPassDescriptor
                    color_attachments = color-attachment
                    depth_stencil_attachment = null

        'set-pipeline rpass render-pipeline
        'set-bind-group rpass 0
            ? (frame-number % 2 == 0) object-bind-group1  object-bind-group2
        'set-bind-group rpass 1 global-bind-group
        'draw rpass 3 1 0 0
        local cmd-buf : wgpu.CommandBuffer rpass

        'submit queue cmd-buf

        'present swap-chain

        glfw.pollEvents;

        let end-time =
            glfw.getTime;

        duration := end-time - start-time
        
        frame-time := 1 / fps

        delay := frame-time - duration
        
        if (frame-number % fps == 0)
            if (delay > 0)
                print "FPS:" fps
                percentage := duration / frame-time * 100.0
                int := percentage as i32
                frac := ((percentage - int as f32) * 100.0) as i32
                print "Workload:"
                    .. (tostring int) "." (tostring frac) "%"
            else
                print "FPS:" (fps / (duration / frame-time))
                print "Workload: 100%"

        if (delay > 0)
            C.usleep (u32 (1000000.0 * delay))
        
        frame-number + 1

static-if main-module?
    try
        main;
    except (error)
else main

