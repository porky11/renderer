using import .shade
using import glm
using import struct
using import ga
using import .shapes
using import .space
using import .camera
using import utils.padding

inout coords : (vector f32 2)
    location = 0

fn vert ()
    let coords... =
        if (VertexID == 0)
            _ 1.0 1.0
        elseif (VertexID == 1)
            _ -3.0 1.0
        else
            _ 1.0 -3.0
    coords.out =
        vectorof f32 coords...
    Position =
        vectorof f32
            call
                va-join coords...
                \ 0.0 1.0

out out-color : (vector f32 4)
    location = 0

uniform window-size :
    struct "window-size"
        window-size : (vector f32 2)
    binding = 0

uniform camera : (Camera D ET)
    binding = 1

struct Shapes
    shapes : (array (Sphere D ET) 0x10)

buffer shape-count :
    struct "ShapeCount"
        count : u32
    binding = 0x10

buffer shapes : Shapes
    binding = 0x11

let epsilon gamma =
    0x0.001 as ET
    0x1000 as ET

fn... field-distance
case (point)
    fold (distance = gamma) for i shape in (enumerate shapes.shapes)
        if (i as u32 == shape-count.count)
            return distance
        let dis =
            'distance (deref (unpad shape)) point
        ? (dis < distance) dis distance
case (point forward)
    fold (distance = gamma) for i shape in (enumerate shapes.shapes)
        if (i as u32 == shape-count.count)
            return distance
        if ('discard? (deref (unpad shape)) point forward)
            distance
        else
            let dis =
                'distance (deref (unpad shape)) point
            if (dis < 0) distance
            else
                ? (dis < distance) dis distance

fn field-normal (point)
    'normalize
        vecof ET
            -
                field-distance
                    vecof ET
                        point.x + epsilon
                        point.y
                        point.z
                field-distance
                    vecof ET
                        point.x - epsilon
                        point.y
                        point.z
            -
                field-distance
                    vecof ET
                        point.x
                        point.y + epsilon
                        point.z
                field-distance
                    vecof ET
                        point.x
                        point.y - epsilon
                        point.z
            -
                field-distance
                    vecof ET
                        point.x
                        point.y
                        point.z + epsilon
                field-distance
                    vecof ET
                        point.x
                        point.y
                        point.z - epsilon

inline unpack-coords ()
    let x y =
        unpack
            coords.in * window-size.window-size
    _ x y

fn plane-mapper ()
    let x y =
        unpack-coords;

    let offset =
        'geometric
            *
                deref camera.dir
                vecof ET x y
            'reverse (deref camera.dir)
            vec D ET

    let view-pos =
        (deref camera.pos) + offset

    let color =
        if ((field-distance view-pos) < 0.0)
            vectorof f32 1 1 1 1
        else
            vectorof f32 0 0 0 1

    out-color = color

fn depth-mapper ()
    let x y =
        unpack-coords;

    let forward =
        'geometric
            *
                deref camera.dir
                'normalize (vecof ET x y -0x400)
            'reverse (deref camera.dir)
            vec D ET

    let view-pos =
        (deref camera.pos) + forward

    let first-distance =
        field-distance view-pos forward

    let color =
        loop (i point distance forward count color color-factor = 0 view-pos first-distance forward 0 0.0 1.0)
            if ((i == 0x40) | (count == 0x10) | (gamma < distance))
                break color
            elseif ((abs distance) < epsilon)
                let normal =
                    field-normal point
                let new-factor =
                    (max 0.0 (- (('scalar forward normal) . e as f32))) * color-factor
                let new-color =
                    new-factor / (2.0 ** count as f32)
                let point =
                    point + normal * epsilon
                let forward =
                    'normalize
                        normal * 2.0 + forward
                _
                    i + 1
                    point
                    field-distance point normal
                    forward
                    count + 1
                    color + new-color
                    new-factor
            else
                let point =
                    point + forward * distance
                _
                    i + 1
                    point
                    field-distance point forward
                    forward
                    count
                    color
                    color-factor

    let color b =
        _
            vectorof f32 1 .5 1 1
            color

    let brightness =
        vectorof f32 b b b 1

    out-color = color * brightness

import wgpu

let vert frag =
    as
        compile-spirv 'vertex (typify vert)
        wgpu.ByteArray
    as
        compile-spirv 'fragment (typify depth-mapper)
        wgpu.ByteArray

locals;
