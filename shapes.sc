using import struct
using import ga
using import utils.utils
using import utils.padding

@@ memo
inline Sphere (D ET)
    @@ aligned
    struct (generic-name "Sphere" D ET) plain
        center :
            vec D ET
        inline discard? (self point forward)
            ('scalar (point - self.center) forward) . e > 0 as ET
        inline distance (self point)
            let T =
                typeof self
            ('norm (point - self.center)) - 1 as ET
        inline normal (self point)
            'normalize (point - self.center)

locals;
