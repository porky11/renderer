import .main

let scope = (Scope)
'bind scope 'main
    static-typify main i32 (pointer rawstring)

compile-object default-target-triple
    compiler-file-kind-object
    module-dir .. "/main.o"
    scope
    'no-debug-info


