# About

Here I implement a signed distance field renderer in [scopes](http://scopes.rocks) using geometric algebra for math and [wgpu](https://github.com/gfx-rs/wgpu) for rendering itself.

Currently there's just support for spheres.
When looking at a shape from inside, you look through it.
You can move and look around.

# Dependencies

There are a few dependencies.

First you need to install [this commit](https://bitbucket.org/duangle/scopes/commits/fe64617885fa9a163d4fdce2f1070a4d11e9cfac) of scopes (it's required to build it yourself; when the next release comes out, this will be used as base).

Then you need to install a few scopes dependencies into your scopes directory:

* [Some utility functions](https://nest.pijul.org/porky11/utils)
* [A geometric algebra library](https://nest.pijul.org/porky11/ga)
* [A library to use custom implementations of mathematical functions](https://nest.pijul.org/porky11/Math)
* [The wgpu wrapper](https://nest.pijul.org/porky11/wgpu)

Besides you need to have the current version of wgpu installed (master branch from the repo) and some version of glfw3.

# Start the program

To start the program just run `scopes main.sc`.

# Controls

You can move around using WASD and QE for moving up and down.
Use the mouse to look around.

# Preview

![Preview](balls.jpg)
