using import enum
using import utils.modify-bindings
using import utils.utils

load-library "/usr/lib/libglfw.so.3"

include (import glfw)
""""#include <GLFW/glfw3.h>
    #define GLFW_EXPOSE_NATIVE_X11
    #define GLFW_EXPOSE_NATIVE_WAYLAND
    #include <GLFW/glfw3native.h>

define glfw
    copy-bindings glfw
        prefix-remover "glfw"
        Scope;

run-stage;

glfw ..
    do
        unlet Error
        enum Error plain
            Init
            Window

        inline setErrorCallback (window callback)
            glfw.setErrorCallback window
                static-typify callback i32 rawstring

        inline setMonitorCallback (window callback)
            glfw.setMonitorCallback window
                static-typify callback
                    mutable pointer glfw.monitor i32

        inline setWindowPosCallback (window callback)
            glfw.setWindowPosCallback window
                static-typify callback
                    mutable pointer glfw.window
                    \ i32 i32

        inline setWindowSizeCallback (window callback)
            glfw.setWindowSizeCallback window
                static-typify callback
                    mutable pointer glfw.window
                    \ i32 i32

        inline setWindowCloseCallback (window callback)
            glfw.setWindowCloseCallback window
                static-typify callback
                    mutable pointer glfw.window

        inline setWindowRefreshCallback (window callback)
            glfw.setWindowRefreshCallback window
                static-typify callback
                    mutable pointer glfw.window

        inline setWindowFocusCallback (window callback)
            glfw.setWindowFocusCallback window
                static-typify callback
                    mutable pointer glfw.window
                    \ i32

        inline setWindowIconifyCallback (window callback)
            glfw.setWindowIconifyCallback window
                static-typify callback
                    mutable pointer glfw.window
                    \ i32

        inline setWindowSetFramebufferSizeCallback (window callback)
            glfw.setWindowSetFramebufferSizeCallback window
                static-typify callback
                    mutable pointer glfw.window
                    \ i32 i32

        inline setKeyCallback (window callback)
            glfw.setKeyCallback window
                static-typify callback
                    mutable pointer glfw.window
                    \ i32 i32 i32 i32

        inline setCharCallback (window callback)
            glfw.setCharCallback window
                static-typify callback
                    mutable pointer glfw.window
                    \ u32

        inline setCharModsCallback (window callback)
            glfw.setCharModsCallback window
                static-typify callback
                    mutable pointer glfw.window
                    \ u32 i32

        inline setMouseButtonCallback (window callback)
            glfw.setMouseButtonCallback window
                static-typify callback
                    mutable pointer glfw.window
                    \ i32 i32 i32

        inline setCurserPosCallback (window callback)
            glfw.setCursorPosCallback window
                static-typify callback
                    mutable pointer glfw.window
                    \ f64 f64

        inline setCurserEnterCallback (window callback)
            glfw.setCursorPosCallback window
                static-typify callback
                    mutable pointer glfw.window
                    \ i32

        inline setDropCallback (window callback)
            glfw.setDropCallback window
                static-typify callback
                    mutable pointer glfw.window
                    \ i32
                    mutable pointer rawstring

        inline setScrollCallback (window callback)
            glfw.setScrollCallback window
                static-typify callback
                    mutable pointer glfw.window
                    \ f64 f64

        inline setJoystickCallback (window callback)
            glfw.setJoystickCallback window
                static-typify callback
                    mutable pointer glfw.window
                    \ i32 i32


        locals;
